/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.util;

/**
 *
 * @author alexandre
 */
public class Constantes {
    
    public static final int STATUS_ID_MATRICULADO = 3;
    public static final int STATUS_ID_TRANCADO = 4;
    public static final int STATUS_ID_CANCELADO = 5;

    public static final int PEFIL_ID_ADMINISTRADOR = 1;
    
    public static final String SESSION_PERFIL_ID = "perfilId";
}
