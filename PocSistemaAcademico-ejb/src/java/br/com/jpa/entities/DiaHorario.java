/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alexandre
 */
@Entity
@Table(name = "dia_horario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DiaHorario.findAll", query = "SELECT d FROM DiaHorario d")
    , @NamedQuery(name = "DiaHorario.findByDiaHorarioId", query = "SELECT d FROM DiaHorario d WHERE d.diaHorarioId = :diaHorarioId")
    , @NamedQuery(name = "DiaHorario.findByDiaSemana", query = "SELECT d FROM DiaHorario d WHERE d.diaSemana = :diaSemana")
    , @NamedQuery(name = "DiaHorario.findByHorario", query = "SELECT d FROM DiaHorario d WHERE d.horario = :horario")})
public class DiaHorario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "dia_horario_id")
    private Integer diaHorarioId;
    @Size(max = 12)
    @Column(name = "dia_semana")
    private String diaSemana;
    @Size(max = 5)
    @Column(name = "horario")
    private String horario;
    @JoinTable(name = "turma_dia_horario", joinColumns = {
        @JoinColumn(name = "dia_horario_id", referencedColumnName = "dia_horario_id")}, inverseJoinColumns = {
        @JoinColumn(name = "turma_id", referencedColumnName = "turma_id")})
    @ManyToMany
    private Collection<Turma> turmaCollection;

    public DiaHorario() {
    }

    public DiaHorario(Integer diaHorarioId) {
        this.diaHorarioId = diaHorarioId;
    }

    public Integer getDiaHorarioId() {
        return diaHorarioId;
    }

    public void setDiaHorarioId(Integer diaHorarioId) {
        this.diaHorarioId = diaHorarioId;
    }

    public String getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(String diaSemana) {
        this.diaSemana = diaSemana;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    @XmlTransient
    public Collection<Turma> getTurmaCollection() {
        return turmaCollection;
    }

    public void setTurmaCollection(Collection<Turma> turmaCollection) {
        this.turmaCollection = turmaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (diaHorarioId != null ? diaHorarioId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DiaHorario)) {
            return false;
        }
        DiaHorario other = (DiaHorario) object;
        if ((this.diaHorarioId == null && other.diaHorarioId != null) || (this.diaHorarioId != null && !this.diaHorarioId.equals(other.diaHorarioId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return diaSemana + " / " + horario;
    }
    
}
