/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alexandre
 */
@Entity
@Table(name = "grade")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Grade.findAll", query = "SELECT g FROM Grade g")
    , @NamedQuery(name = "Grade.findByGradeId", query = "SELECT g FROM Grade g WHERE g.gradeId = :gradeId")
    , @NamedQuery(name = "Grade.findByDescricao", query = "SELECT g FROM Grade g WHERE g.descricao = :descricao")
    , @NamedQuery(name = "Grade.findByDataInicial", query = "SELECT g FROM Grade g WHERE g.dataInicial = :dataInicial")
    , @NamedQuery(name = "Grade.findByDataFinal", query = "SELECT g FROM Grade g WHERE g.dataFinal = :dataFinal")})
public class Grade implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "grade_id")
    private Integer gradeId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "descricao")
    private String descricao;
    @Basic(optional = false)
    @NotNull
    @Column(name = "data_inicial")
    @Temporal(TemporalType.DATE)
    private Date dataInicial;
    @Column(name = "data_final")
    @Temporal(TemporalType.DATE)
    private Date dataFinal;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grade")
    private Collection<GradeCursoMateria> gradeCursoMateriaCollection;

    public Grade() {
    }

    public Grade(Integer gradeId) {
        this.gradeId = gradeId;
    }

    public Grade(Integer gradeId, String descricao, Date dataInicial) {
        this.gradeId = gradeId;
        this.descricao = descricao;
        this.dataInicial = dataInicial;
    }

    public Integer getGradeId() {
        return gradeId;
    }

    public void setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    @XmlTransient
    public Collection<GradeCursoMateria> getGradeCursoMateriaCollection() {
        return gradeCursoMateriaCollection;
    }

    public void setGradeCursoMateriaCollection(Collection<GradeCursoMateria> gradeCursoMateriaCollection) {
        this.gradeCursoMateriaCollection = gradeCursoMateriaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gradeId != null ? gradeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grade)) {
            return false;
        }
        Grade other = (Grade) object;
        if ((this.gradeId == null && other.gradeId != null) || (this.gradeId != null && !this.gradeId.equals(other.gradeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return gradeId + " - " + descricao;
    }
    
}
