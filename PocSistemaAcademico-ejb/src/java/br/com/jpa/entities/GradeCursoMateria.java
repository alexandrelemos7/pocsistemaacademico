/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.entities;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alexandre
 */
@Entity
@Table(name = "grade_curso_materia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GradeCursoMateria.findAll", query = "SELECT g FROM GradeCursoMateria g")
    , @NamedQuery(name = "GradeCursoMateria.findByGradeId", query = "SELECT g FROM GradeCursoMateria g WHERE g.gradeCursoMateriaPK.gradeId = :gradeId")
    , @NamedQuery(name = "GradeCursoMateria.findByCursoId", query = "SELECT g FROM GradeCursoMateria g WHERE g.gradeCursoMateriaPK.cursoId = :cursoId")
    , @NamedQuery(name = "GradeCursoMateria.findByMateriaId", query = "SELECT g FROM GradeCursoMateria g WHERE g.gradeCursoMateriaPK.materiaId = :materiaId")})
public class GradeCursoMateria implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GradeCursoMateriaPK gradeCursoMateriaPK;
    @JoinColumn(name = "curso_id", referencedColumnName = "curso_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Curso curso;
    @JoinColumn(name = "grade_id", referencedColumnName = "grade_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Grade grade;
    @JoinColumn(name = "materia_id", referencedColumnName = "materia_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Materia materia;

    public GradeCursoMateria() {
    }

    public GradeCursoMateria(GradeCursoMateriaPK gradeCursoMateriaPK) {
        this.gradeCursoMateriaPK = gradeCursoMateriaPK;
    }

    public GradeCursoMateria(int gradeId, int cursoId, int materiaId) {
        this.gradeCursoMateriaPK = new GradeCursoMateriaPK(gradeId, cursoId, materiaId);
    }

    public GradeCursoMateriaPK getGradeCursoMateriaPK() {
        return gradeCursoMateriaPK;
    }

    public void setGradeCursoMateriaPK(GradeCursoMateriaPK gradeCursoMateriaPK) {
        this.gradeCursoMateriaPK = gradeCursoMateriaPK;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public Grade getGrade() {
        return grade;
    }

    public void setGrade(Grade grade) {
        this.grade = grade;
    }

    public Materia getMateria() {
        return materia;
    }

    public void setMateria(Materia materia) {
        this.materia = materia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gradeCursoMateriaPK != null ? gradeCursoMateriaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GradeCursoMateria)) {
            return false;
        }
        GradeCursoMateria other = (GradeCursoMateria) object;
        if ((this.gradeCursoMateriaPK == null && other.gradeCursoMateriaPK != null) || (this.gradeCursoMateriaPK != null && !this.gradeCursoMateriaPK.equals(other.gradeCursoMateriaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return grade.getDescricao() + " - " + curso.getDescricao() + " - " + materia.getDescricao();
    }
    
}
