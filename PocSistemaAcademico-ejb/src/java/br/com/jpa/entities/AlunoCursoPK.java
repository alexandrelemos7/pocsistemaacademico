/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author alexandre
 */
@Embeddable
public class AlunoCursoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "aluno_id")
    private int alunoId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "curso_id")
    private int cursoId;

    public AlunoCursoPK() {
    }

    public AlunoCursoPK(int alunoId, int cursoId) {
        this.alunoId = alunoId;
        this.cursoId = cursoId;
    }

    public int getAlunoId() {
        return alunoId;
    }

    public void setAlunoId(int alunoId) {
        this.alunoId = alunoId;
    }

    public int getCursoId() {
        return cursoId;
    }

    public void setCursoId(int cursoId) {
        this.cursoId = cursoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) alunoId;
        hash += (int) cursoId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AlunoCursoPK)) {
            return false;
        }
        AlunoCursoPK other = (AlunoCursoPK) object;
        if (this.alunoId != other.alunoId) {
            return false;
        }
        if (this.cursoId != other.cursoId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.jpa.entities.AlunoCursoPK[ alunoId=" + alunoId + ", cursoId=" + cursoId + " ]";
    }
    
}
