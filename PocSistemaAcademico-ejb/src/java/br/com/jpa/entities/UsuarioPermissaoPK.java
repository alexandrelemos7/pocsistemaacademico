/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alexandre
 */
@Embeddable
public class UsuarioPermissaoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "usuario_id")
    private int usuarioId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "permissao")
    private String permissao;

    public UsuarioPermissaoPK() {
    }

    public UsuarioPermissaoPK(int usuarioId, String permissao) {
        this.usuarioId = usuarioId;
        this.permissao = permissao;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getPermissao() {
        return permissao;
    }

    public void setPermissao(String permissao) {
        this.permissao = permissao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) usuarioId;
        hash += (permissao != null ? permissao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioPermissaoPK)) {
            return false;
        }
        UsuarioPermissaoPK other = (UsuarioPermissaoPK) object;
        if (this.usuarioId != other.usuarioId) {
            return false;
        }
        if ((this.permissao == null && other.permissao != null) || (this.permissao != null && !this.permissao.equals(other.permissao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.jpa.entities.UsuarioPermissaoPK[ usuarioId=" + usuarioId + ", permissao=" + permissao + " ]";
    }
    
}
