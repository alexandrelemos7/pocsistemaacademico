/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alexandre
 */
@Entity
@Table(name = "materia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Materia.findAll", query = "SELECT m FROM Materia m")
    , @NamedQuery(name = "Materia.findByMateriaId", query = "SELECT m FROM Materia m WHERE m.materiaId = :materiaId")
    , @NamedQuery(name = "Materia.findByDescricao", query = "SELECT m FROM Materia m WHERE m.descricao = :descricao")})
public class Materia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "materia_id")
    private Integer materiaId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "descricao")
    private String descricao;
    @JoinTable(name = "materia_pre_requisito", joinColumns = {
        @JoinColumn(name = "materia_id", referencedColumnName = "materia_id")}, inverseJoinColumns = {
        @JoinColumn(name = "materia_pre_requisito_id", referencedColumnName = "materia_id")})
    @ManyToMany
    private Collection<Materia> materiaCollection;
    @ManyToMany(mappedBy = "materiaCollection")
    private Collection<Materia> materiaCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "materiaId")
    private Collection<Turma> turmaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "materia")
    private Collection<GradeCursoMateria> gradeCursoMateriaCollection;

    public Materia() {
    }

    public Materia(Integer materiaId) {
        this.materiaId = materiaId;
    }

    public Materia(Integer materiaId, String descricao) {
        this.materiaId = materiaId;
        this.descricao = descricao;
    }

    public Integer getMateriaId() {
        return materiaId;
    }

    public void setMateriaId(Integer materiaId) {
        this.materiaId = materiaId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @XmlTransient
    public Collection<Materia> getMateriaCollection() {
        return materiaCollection;
    }

    public void setMateriaCollection(Collection<Materia> materiaCollection) {
        this.materiaCollection = materiaCollection;
    }

    @XmlTransient
    public Collection<Materia> getMateriaCollection1() {
        return materiaCollection1;
    }

    public void setMateriaCollection1(Collection<Materia> materiaCollection1) {
        this.materiaCollection1 = materiaCollection1;
    }

    @XmlTransient
    public Collection<Turma> getTurmaCollection() {
        return turmaCollection;
    }

    public void setTurmaCollection(Collection<Turma> turmaCollection) {
        this.turmaCollection = turmaCollection;
    }

    @XmlTransient
    public Collection<GradeCursoMateria> getGradeCursoMateriaCollection() {
        return gradeCursoMateriaCollection;
    }

    public void setGradeCursoMateriaCollection(Collection<GradeCursoMateria> gradeCursoMateriaCollection) {
        this.gradeCursoMateriaCollection = gradeCursoMateriaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (materiaId != null ? materiaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Materia)) {
            return false;
        }
        Materia other = (Materia) object;
        if ((this.materiaId == null && other.materiaId != null) || (this.materiaId != null && !this.materiaId.equals(other.materiaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return materiaId + " - " + descricao;
    }
    
}
