/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.entities;

import br.com.util.Constantes;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alexandre
 */
@Entity
@Table(name = "matricula")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Matricula.findAll", query = "SELECT m FROM Matricula m")
    , @NamedQuery(name = "Matricula.findByMatriculaId", query = "SELECT m FROM Matricula m WHERE m.matriculaId = :matriculaId")
    , @NamedQuery(name = "Matricula.findByDataMatricula", query = "SELECT m FROM Matricula m WHERE m.dataMatricula = :dataMatricula")
    , @NamedQuery(name = "Matricula.findAllByAluno", query = "SELECT m FROM Matricula m WHERE m.alunoId.usuarioId.nome = :usuarioNome AND m.turmaId.dataFinal > CURRENT_DATE")})
public class Matricula implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "matricula_id")
    private Integer matriculaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "data_matricula")
    @Temporal(TemporalType.DATE)
    private Date dataMatricula;
    @Column(name = "data_trancamento")
    @Temporal(TemporalType.DATE)
    private Date dataTrancamento;
    @Column(name = "data_cancelamento")
    @Temporal(TemporalType.DATE)
    private Date dataCancelamento;
    @JoinColumn(name = "aluno_id", referencedColumnName = "aluno_id")
    @ManyToOne(optional = false)
    private Aluno alunoId;
    @JoinColumn(name = "status_id", referencedColumnName = "status_id")
    @ManyToOne(optional = false)
    private Status statusId;
    @JoinColumn(name = "turma_id", referencedColumnName = "turma_id")
    @ManyToOne(optional = false)
    private Turma turmaId;
    
    @Transient
    private boolean cancelar;
    @Transient
    private boolean trancar;

    public Matricula() {
    }

    public Matricula(Integer matriculaId) {
        this.matriculaId = matriculaId;
    }

    public Matricula(Integer matriculaId, Date dataMatricula) {
        this.matriculaId = matriculaId;
        this.dataMatricula = dataMatricula;
    }

    public Integer getMatriculaId() {
        return matriculaId;
    }

    public void setMatriculaId(Integer matriculaId) {
        this.matriculaId = matriculaId;
    }

    public Date getDataMatricula() {
        return dataMatricula;
    }

    public void setDataMatricula(Date dataMatricula) {
        this.dataMatricula = dataMatricula;
    }
    
    public Date getDataTrancamento() {
        return dataTrancamento;
    }

    public void setDataTrancamento(Date dataTrancamento) {
        this.dataTrancamento = dataTrancamento;
    }

    
    public Date getDataCancelamento() {
        return dataCancelamento;
    }

    public void setDataCancelamento(Date dataCancelamento) {
        this.dataCancelamento = dataCancelamento;
    }


    public Aluno getAlunoId() {
        return alunoId;
    }

    public void setAlunoId(Aluno alunoId) {
        this.alunoId = alunoId;
    }

    public Status getStatusId() {
        return statusId;
    }

    public void setStatusId(Status statusId) {
        this.statusId = statusId;
    }

    public Turma getTurmaId() {
        return turmaId;
    }

    public void setTurmaId(Turma turmaId) {
        this.turmaId = turmaId;
    }
    
    public boolean getCancelar() {
        Date dataAtual = new Date();
        return Constantes.STATUS_ID_MATRICULADO == statusId.getStatusId() && 0 > dataAtual.compareTo(turmaId.getDataLimiteInscricao());
    }
    
    public boolean getTrancar() {
        Date dataAtual = new Date();
        return Constantes.STATUS_ID_MATRICULADO == statusId.getStatusId() && 0 <= dataAtual.compareTo(turmaId.getDataLimiteInscricao());
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (matriculaId != null ? matriculaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Matricula)) {
            return false;
        }
        Matricula other = (Matricula) object;
        if ((this.matriculaId == null && other.matriculaId != null) || (this.matriculaId != null && !this.matriculaId.equals(other.matriculaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return alunoId.getNome() + " - " + turmaId.getMateriaId().getDescricao();
    }
    
}
