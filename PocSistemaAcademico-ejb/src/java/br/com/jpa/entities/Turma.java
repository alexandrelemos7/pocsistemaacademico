/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alexandre
 */
@Entity
@Table(name = "turma")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Turma.findAll", query = "SELECT t FROM Turma t")
    , @NamedQuery(name = "Turma.findByTurmaId", query = "SELECT t FROM Turma t WHERE t.turmaId = :turmaId")
    , @NamedQuery(name = "Turma.findByDataLimiteInscricao", query = "SELECT t FROM Turma t WHERE t.dataLimiteInscricao = :dataLimiteInscricao")
    , @NamedQuery(name = "Turma.findByQuantidadeVagasDisponivel", query = "SELECT DISTINCT t "+
            "FROM Turma t "+
            "WHERE t.quantidadeVagasDisponivel > (SELECT COUNT(m.matriculaId) FROM Matricula m WHERE t.turmaId = m.turmaId.turmaId) "+
            "    AND t.materiaId.materiaId IN(SELECT gcm.materia.materiaId "+
            "        FROM GradeCursoMateria gcm "+
            "        WHERE gcm.curso.cursoId IN(SELECT ac.curso.cursoId "+
            "            FROM AlunoCurso ac "+
            "            WHERE ac.aluno.usuarioId.nome = :usuarioNome))"
            )})
public class Turma implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "data_inicio")
    @Temporal(TemporalType.DATE)
    private Date dataInicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "data_final")
    @Temporal(TemporalType.DATE)
    private Date dataFinal;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "turma_id")
    private Integer turmaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "data_limite_inscricao")
    @Temporal(TemporalType.DATE)
    private Date dataLimiteInscricao;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantidade_vagas_disponivel")
    private int quantidadeVagasDisponivel;
    @ManyToMany(mappedBy = "turmaCollection")
    private Collection<DiaHorario> diaHorarioCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "turmaId")
    private Collection<Matricula> matriculaCollection;
    @JoinColumn(name = "materia_id", referencedColumnName = "materia_id")
    @ManyToOne(optional = false)
    private Materia materiaId;
    @JoinColumn(name = "professor_id", referencedColumnName = "professor_id")
    @ManyToOne(optional = false)
    private Professor professorId;

    public Turma() {
    }

    public Turma(Integer turmaId) {
        this.turmaId = turmaId;
    }

    public Turma(Integer turmaId, Date dataLimiteInscricao, int quantidadeVagasDisponivel) {
        this.turmaId = turmaId;
        this.dataLimiteInscricao = dataLimiteInscricao;
        this.quantidadeVagasDisponivel = quantidadeVagasDisponivel;
    }

    public Integer getTurmaId() {
        return turmaId;
    }

    public void setTurmaId(Integer turmaId) {
        this.turmaId = turmaId;
    }

    public Date getDataLimiteInscricao() {
        return dataLimiteInscricao;
    }

    public void setDataLimiteInscricao(Date dataLimiteInscricao) {
        this.dataLimiteInscricao = dataLimiteInscricao;
    }

    public int getQuantidadeVagasDisponivel() {
        return quantidadeVagasDisponivel;
    }

    public void setQuantidadeVagasDisponivel(int quantidadeVagasDisponivel) {
        this.quantidadeVagasDisponivel = quantidadeVagasDisponivel;
    }

    @XmlTransient
    public Collection<DiaHorario> getDiaHorarioCollection() {
        return diaHorarioCollection;
    }

    public void setDiaHorarioCollection(Collection<DiaHorario> diaHorarioCollection) {
        this.diaHorarioCollection = diaHorarioCollection;
    }

    @XmlTransient
    public Collection<Matricula> getMatriculaCollection() {
        return matriculaCollection;
    }

    public void setMatriculaCollection(Collection<Matricula> matriculaCollection) {
        this.matriculaCollection = matriculaCollection;
    }

    public Materia getMateriaId() {
        return materiaId;
    }

    public void setMateriaId(Materia materiaId) {
        this.materiaId = materiaId;
    }

    public Professor getProfessorId() {
        return professorId;
    }

    public void setProfessorId(Professor professorId) {
        this.professorId = professorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (turmaId != null ? turmaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Turma)) {
            return false;
        }
        Turma other = (Turma) object;
        if ((this.turmaId == null && other.turmaId != null) || (this.turmaId != null && !this.turmaId.equals(other.turmaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return turmaId + " / " + materiaId.getMateriaId() + " - " + materiaId.getDescricao();
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }
    
}
