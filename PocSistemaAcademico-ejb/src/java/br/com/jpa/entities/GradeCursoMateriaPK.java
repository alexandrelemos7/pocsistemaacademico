/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author alexandre
 */
@Embeddable
public class GradeCursoMateriaPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "grade_id")
    private int gradeId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "curso_id")
    private int cursoId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "materia_id")
    private int materiaId;

    public GradeCursoMateriaPK() {
    }

    public GradeCursoMateriaPK(int gradeId, int cursoId, int materiaId) {
        this.gradeId = gradeId;
        this.cursoId = cursoId;
        this.materiaId = materiaId;
    }

    public int getGradeId() {
        return gradeId;
    }

    public void setGradeId(int gradeId) {
        this.gradeId = gradeId;
    }

    public int getCursoId() {
        return cursoId;
    }

    public void setCursoId(int cursoId) {
        this.cursoId = cursoId;
    }

    public int getMateriaId() {
        return materiaId;
    }

    public void setMateriaId(int materiaId) {
        this.materiaId = materiaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) gradeId;
        hash += (int) cursoId;
        hash += (int) materiaId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GradeCursoMateriaPK)) {
            return false;
        }
        GradeCursoMateriaPK other = (GradeCursoMateriaPK) object;
        if (this.gradeId != other.gradeId) {
            return false;
        }
        if (this.cursoId != other.cursoId) {
            return false;
        }
        if (this.materiaId != other.materiaId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.com.jpa.entities.GradeCursoMateriaPK[ gradeId=" + gradeId + ", cursoId=" + cursoId + ", materiaId=" + materiaId + " ]";
    }
    
}
