/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.entities;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alexandre
 */
@Entity
@Table(name = "usuario_permissao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UsuarioPermissao.findAll", query = "SELECT u FROM UsuarioPermissao u")
    , @NamedQuery(name = "UsuarioPermissao.findByUsuarioId", query = "SELECT u FROM UsuarioPermissao u WHERE u.usuarioPermissaoPK.usuarioId = :usuarioId")
    , @NamedQuery(name = "UsuarioPermissao.findByPermissao", query = "SELECT u FROM UsuarioPermissao u WHERE u.usuarioPermissaoPK.permissao = :permissao")})
public class UsuarioPermissao implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsuarioPermissaoPK usuarioPermissaoPK;
    @JoinColumn(name = "usuario_id", referencedColumnName = "usuario_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Usuario usuario;

    public UsuarioPermissao() {
    }

    public UsuarioPermissao(UsuarioPermissaoPK usuarioPermissaoPK) {
        this.usuarioPermissaoPK = usuarioPermissaoPK;
    }

    public UsuarioPermissao(int usuarioId, String permissao) {
        this.usuarioPermissaoPK = new UsuarioPermissaoPK(usuarioId, permissao);
    }

    public UsuarioPermissaoPK getUsuarioPermissaoPK() {
        return usuarioPermissaoPK;
    }

    public void setUsuarioPermissaoPK(UsuarioPermissaoPK usuarioPermissaoPK) {
        this.usuarioPermissaoPK = usuarioPermissaoPK;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuarioPermissaoPK != null ? usuarioPermissaoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioPermissao)) {
            return false;
        }
        UsuarioPermissao other = (UsuarioPermissao) object;
        if ((this.usuarioPermissaoPK == null && other.usuarioPermissaoPK != null) || (this.usuarioPermissaoPK != null && !this.usuarioPermissaoPK.equals(other.usuarioPermissaoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return usuario.getNome() + " / " + usuarioPermissaoPK.getPermissao();
    }
    
}
