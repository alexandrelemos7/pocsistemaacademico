/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alexandre
 */
@Entity
@Table(name = "aluno_curso")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AlunoCurso.findAll", query = "SELECT a FROM AlunoCurso a")
    , @NamedQuery(name = "AlunoCurso.findByAlunoId", query = "SELECT a FROM AlunoCurso a WHERE a.alunoCursoPK.alunoId = :alunoId")
    , @NamedQuery(name = "AlunoCurso.findByCursoId", query = "SELECT a FROM AlunoCurso a WHERE a.alunoCursoPK.cursoId = :cursoId")
    , @NamedQuery(name = "AlunoCurso.findByDataInicioCurso", query = "SELECT a FROM AlunoCurso a WHERE a.dataInicioCurso = :dataInicioCurso")
    , @NamedQuery(name = "AlunoCurso.findByDataPrevisaoConclusao", query = "SELECT a FROM AlunoCurso a WHERE a.dataPrevisaoConclusao = :dataPrevisaoConclusao")})
public class AlunoCurso implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AlunoCursoPK alunoCursoPK;
    @Column(name = "data_inicio_curso")
    @Temporal(TemporalType.DATE)
    private Date dataInicioCurso;
    @Column(name = "data_previsao_conclusao")
    @Temporal(TemporalType.DATE)
    private Date dataPrevisaoConclusao;
    @JoinColumn(name = "aluno_id", referencedColumnName = "aluno_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Aluno aluno;
    @JoinColumn(name = "curso_id", referencedColumnName = "curso_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Curso curso;

    public AlunoCurso() {
    }

    public AlunoCurso(AlunoCursoPK alunoCursoPK) {
        this.alunoCursoPK = alunoCursoPK;
    }

    public AlunoCurso(int alunoId, int cursoId) {
        this.alunoCursoPK = new AlunoCursoPK(alunoId, cursoId);
    }

    public AlunoCursoPK getAlunoCursoPK() {
        return alunoCursoPK;
    }

    public void setAlunoCursoPK(AlunoCursoPK alunoCursoPK) {
        this.alunoCursoPK = alunoCursoPK;
    }

    public Date getDataInicioCurso() {
        return dataInicioCurso;
    }

    public void setDataInicioCurso(Date dataInicioCurso) {
        this.dataInicioCurso = dataInicioCurso;
    }

    public Date getDataPrevisaoConclusao() {
        return dataPrevisaoConclusao;
    }

    public void setDataPrevisaoConclusao(Date dataPrevisaoConclusao) {
        this.dataPrevisaoConclusao = dataPrevisaoConclusao;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (alunoCursoPK != null ? alunoCursoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AlunoCurso)) {
            return false;
        }
        AlunoCurso other = (AlunoCurso) object;
        if ((this.alunoCursoPK == null && other.alunoCursoPK != null) || (this.alunoCursoPK != null && !this.alunoCursoPK.equals(other.alunoCursoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return aluno.getAlunoId() + " - " + aluno.getNome() + " / " + curso.getCursoId() + " - " + curso.getDescricao();
    }
    
}
