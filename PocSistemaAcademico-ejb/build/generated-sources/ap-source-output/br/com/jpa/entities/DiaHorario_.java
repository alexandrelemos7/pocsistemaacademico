package br.com.jpa.entities;

import br.com.jpa.entities.Turma;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-18T00:40:34")
@StaticMetamodel(DiaHorario.class)
public class DiaHorario_ { 

    public static volatile SingularAttribute<DiaHorario, String> diaSemana;
    public static volatile SingularAttribute<DiaHorario, String> horario;
    public static volatile CollectionAttribute<DiaHorario, Turma> turmaCollection;
    public static volatile SingularAttribute<DiaHorario, Integer> diaHorarioId;

}