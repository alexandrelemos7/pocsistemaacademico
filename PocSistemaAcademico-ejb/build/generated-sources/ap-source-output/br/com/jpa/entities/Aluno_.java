package br.com.jpa.entities;

import br.com.jpa.entities.AlunoCurso;
import br.com.jpa.entities.Matricula;
import br.com.jpa.entities.Usuario;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-18T00:40:34")
@StaticMetamodel(Aluno.class)
public class Aluno_ { 

    public static volatile CollectionAttribute<Aluno, AlunoCurso> alunoCursoCollection;
    public static volatile SingularAttribute<Aluno, Integer> alunoId;
    public static volatile SingularAttribute<Aluno, String> nome;
    public static volatile CollectionAttribute<Aluno, Matricula> matriculaCollection;
    public static volatile SingularAttribute<Aluno, Date> dataNascimento;
    public static volatile SingularAttribute<Aluno, Usuario> usuarioId;

}