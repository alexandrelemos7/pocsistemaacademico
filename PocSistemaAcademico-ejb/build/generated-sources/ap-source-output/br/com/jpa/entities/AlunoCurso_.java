package br.com.jpa.entities;

import br.com.jpa.entities.Aluno;
import br.com.jpa.entities.AlunoCursoPK;
import br.com.jpa.entities.Curso;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-18T00:40:34")
@StaticMetamodel(AlunoCurso.class)
public class AlunoCurso_ { 

    public static volatile SingularAttribute<AlunoCurso, Aluno> aluno;
    public static volatile SingularAttribute<AlunoCurso, Date> dataInicioCurso;
    public static volatile SingularAttribute<AlunoCurso, AlunoCursoPK> alunoCursoPK;
    public static volatile SingularAttribute<AlunoCurso, Curso> curso;
    public static volatile SingularAttribute<AlunoCurso, Date> dataPrevisaoConclusao;

}