package br.com.jpa.entities;

import br.com.jpa.entities.Curso;
import br.com.jpa.entities.Grade;
import br.com.jpa.entities.GradeCursoMateriaPK;
import br.com.jpa.entities.Materia;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-18T00:40:34")
@StaticMetamodel(GradeCursoMateria.class)
public class GradeCursoMateria_ { 

    public static volatile SingularAttribute<GradeCursoMateria, Curso> curso;
    public static volatile SingularAttribute<GradeCursoMateria, Grade> grade;
    public static volatile SingularAttribute<GradeCursoMateria, Materia> materia;
    public static volatile SingularAttribute<GradeCursoMateria, GradeCursoMateriaPK> gradeCursoMateriaPK;

}