package br.com.jpa.entities;

import br.com.jpa.entities.Aluno;
import br.com.jpa.entities.Status;
import br.com.jpa.entities.Turma;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-18T00:40:34")
@StaticMetamodel(Matricula.class)
public class Matricula_ { 

    public static volatile SingularAttribute<Matricula, Turma> turmaId;
    public static volatile SingularAttribute<Matricula, Integer> matriculaId;
    public static volatile SingularAttribute<Matricula, Date> dataTrancamento;
    public static volatile SingularAttribute<Matricula, Status> statusId;
    public static volatile SingularAttribute<Matricula, Date> dataMatricula;
    public static volatile SingularAttribute<Matricula, Aluno> alunoId;
    public static volatile SingularAttribute<Matricula, Date> dataCancelamento;

}