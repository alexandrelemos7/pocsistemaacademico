package br.com.jpa.entities;

import br.com.jpa.entities.AlunoCurso;
import br.com.jpa.entities.GradeCursoMateria;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-18T00:40:34")
@StaticMetamodel(Curso.class)
public class Curso_ { 

    public static volatile CollectionAttribute<Curso, AlunoCurso> alunoCursoCollection;
    public static volatile SingularAttribute<Curso, Integer> cursoId;
    public static volatile CollectionAttribute<Curso, GradeCursoMateria> gradeCursoMateriaCollection;
    public static volatile SingularAttribute<Curso, String> descricao;

}