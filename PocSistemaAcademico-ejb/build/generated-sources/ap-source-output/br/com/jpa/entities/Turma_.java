package br.com.jpa.entities;

import br.com.jpa.entities.DiaHorario;
import br.com.jpa.entities.Materia;
import br.com.jpa.entities.Matricula;
import br.com.jpa.entities.Professor;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-18T00:40:34")
@StaticMetamodel(Turma.class)
public class Turma_ { 

    public static volatile SingularAttribute<Turma, Integer> turmaId;
    public static volatile SingularAttribute<Turma, Date> dataLimiteInscricao;
    public static volatile SingularAttribute<Turma, Integer> quantidadeVagasDisponivel;
    public static volatile SingularAttribute<Turma, Materia> materiaId;
    public static volatile CollectionAttribute<Turma, Matricula> matriculaCollection;
    public static volatile SingularAttribute<Turma, Date> dataInicio;
    public static volatile SingularAttribute<Turma, Professor> professorId;
    public static volatile SingularAttribute<Turma, Date> dataFinal;
    public static volatile CollectionAttribute<Turma, DiaHorario> diaHorarioCollection;

}