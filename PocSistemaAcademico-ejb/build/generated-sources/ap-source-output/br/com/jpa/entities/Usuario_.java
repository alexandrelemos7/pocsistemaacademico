package br.com.jpa.entities;

import br.com.jpa.entities.Aluno;
import br.com.jpa.entities.Perfil;
import br.com.jpa.entities.Professor;
import br.com.jpa.entities.UsuarioPermissao;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-18T00:40:34")
@StaticMetamodel(Usuario.class)
public class Usuario_ { 

    public static volatile SingularAttribute<Usuario, String> senha;
    public static volatile CollectionAttribute<Usuario, UsuarioPermissao> usuarioPermissaoCollection;
    public static volatile CollectionAttribute<Usuario, Aluno> alunoCollection;
    public static volatile CollectionAttribute<Usuario, Professor> professorCollection;
    public static volatile SingularAttribute<Usuario, Perfil> perfilId;
    public static volatile SingularAttribute<Usuario, String> nome;
    public static volatile SingularAttribute<Usuario, Integer> usuarioId;

}