package br.com.jpa.entities;

import br.com.jpa.entities.Usuario;
import br.com.jpa.entities.UsuarioPermissaoPK;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-18T00:40:34")
@StaticMetamodel(UsuarioPermissao.class)
public class UsuarioPermissao_ { 

    public static volatile SingularAttribute<UsuarioPermissao, Usuario> usuario;
    public static volatile SingularAttribute<UsuarioPermissao, UsuarioPermissaoPK> usuarioPermissaoPK;

}