package br.com.jpa.entities;

import br.com.jpa.entities.GradeCursoMateria;
import br.com.jpa.entities.Materia;
import br.com.jpa.entities.Turma;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-18T00:40:34")
@StaticMetamodel(Materia.class)
public class Materia_ { 

    public static volatile CollectionAttribute<Materia, Materia> materiaCollection1;
    public static volatile CollectionAttribute<Materia, Turma> turmaCollection;
    public static volatile SingularAttribute<Materia, Integer> materiaId;
    public static volatile CollectionAttribute<Materia, Materia> materiaCollection;
    public static volatile CollectionAttribute<Materia, GradeCursoMateria> gradeCursoMateriaCollection;
    public static volatile SingularAttribute<Materia, String> descricao;

}