package br.com.jpa.entities;

import br.com.jpa.entities.Matricula;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-18T00:40:34")
@StaticMetamodel(Status.class)
public class Status_ { 

    public static volatile SingularAttribute<Status, Integer> statusId;
    public static volatile CollectionAttribute<Status, Matricula> matriculaCollection;
    public static volatile SingularAttribute<Status, String> descricao;

}