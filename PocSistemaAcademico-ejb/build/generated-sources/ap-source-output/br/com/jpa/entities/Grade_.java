package br.com.jpa.entities;

import br.com.jpa.entities.GradeCursoMateria;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-18T00:40:34")
@StaticMetamodel(Grade.class)
public class Grade_ { 

    public static volatile SingularAttribute<Grade, Integer> gradeId;
    public static volatile SingularAttribute<Grade, Date> dataInicial;
    public static volatile CollectionAttribute<Grade, GradeCursoMateria> gradeCursoMateriaCollection;
    public static volatile SingularAttribute<Grade, Date> dataFinal;
    public static volatile SingularAttribute<Grade, String> descricao;

}