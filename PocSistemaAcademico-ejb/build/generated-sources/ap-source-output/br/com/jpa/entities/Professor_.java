package br.com.jpa.entities;

import br.com.jpa.entities.Turma;
import br.com.jpa.entities.Usuario;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-09-18T00:40:34")
@StaticMetamodel(Professor.class)
public class Professor_ { 

    public static volatile CollectionAttribute<Professor, Turma> turmaCollection;
    public static volatile SingularAttribute<Professor, String> nome;
    public static volatile SingularAttribute<Professor, Integer> professorId;
    public static volatile SingularAttribute<Professor, Date> dataNascimento;
    public static volatile SingularAttribute<Professor, Usuario> usuarioId;

}