## Trabalho de conclusão de curso - Pós-graduação PUC Minas - Poc Sistema Acadêmico

Para desenvolvimento da prova de conceito foram utilizados a IDE Netbeans 8.2, Glassfish 4.1.2, JSF 2, EclipseLink JPA 2.1, EJB 3.1, MySql 5.7.19,  sistema operacional Linux 64 Bits e Java 1.8.

## Script para criação da base de dados

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS pocacademico;
CREATE SCHEMA pocacademico;
USE pocacademico;

CREATE TABLE perfil (
  perfil_id INTEGER NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(50) NOT NULL,
  CONSTRAINT perfil_pk PRIMARY KEY (perfil_id),
  UNIQUE KEY nome_UNIQUE (descricao)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE usuario (
  usuario_id INTEGER NOT NULL AUTO_INCREMENT,
  nome VARCHAR(50) NOT NULL,
  senha VARCHAR(100) NOT NULL,
  perfil_id INTEGER,
  CONSTRAINT usuario_pk PRIMARY KEY (usuario_id),
  UNIQUE KEY nome_UNIQUE (nome)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE usuario_permissao (
  usuario_id INTEGER NOT NULL,
  permissao VARCHAR(50) NOT NULL,
  CONSTRAINT usuario_permissao_pk PRIMARY KEY (usuario_id, permissao)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE aluno (
	aluno_id INTEGER NOT NULL AUTO_INCREMENT,
	nome VARCHAR(50) NOT NULL,
	data_nascimento DATE NOT NULL,
	usuario_id INTEGER,
	CONSTRAINT aluno_pk PRIMARY KEY ( aluno_id )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE curso (
	curso_id INTEGER NOT NULL AUTO_INCREMENT,
	descricao VARCHAR(50) NOT NULL,
	CONSTRAINT curso_pk PRIMARY KEY ( curso_id )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table aluno_curso(
    aluno_id INTEGER NOT NULL,
    curso_id INTEGER NOT NULL,
    data_inicio_curso DATE NULL,
    data_previsao_conclusao DATE NULL,
    CONSTRAINT aluno_curso_pk PRIMARY KEY ( aluno_id, curso_id )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE materia (
	materia_id INTEGER NOT NULL AUTO_INCREMENT,
	descricao VARCHAR(50) NOT NULL,
	CONSTRAINT materia_pk PRIMARY KEY ( materia_id )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE materia_pre_requisito (
	materia_id INTEGER NOT NULL,
	materia_pre_requisito_id INTEGER NOT NULL,
	CONSTRAINT materia_pre_requisito_pk PRIMARY KEY ( materia_id, materia_pre_requisito_id )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE grade (
	grade_id INTEGER NOT NULL AUTO_INCREMENT,
	descricao VARCHAR(50) NOT NULL,
	data_inicial DATE NOT NULL,
	data_final DATE NULL,
	CONSTRAINT grade_pk PRIMARY KEY ( grade_id )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE grade_curso_materia (
	grade_id INTEGER NOT NULL,
	curso_id INTEGER NOT NULL,
	materia_id INTEGER NOT NULL,
	CONSTRAINT grade_pk PRIMARY KEY ( grade_id, curso_id, materia_id )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE professor (
	professor_id INTEGER NOT NULL AUTO_INCREMENT,
	nome VARCHAR(50) NOT NULL,
	data_nascimento DATE NOT NULL,
	usuario_id INTEGER,
	CONSTRAINT professor_pk PRIMARY KEY ( professor_id )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE dia_horario (
	dia_horario_id INTEGER NOT NULL AUTO_INCREMENT,
	dia_semana VARCHAR(12),
	horario VARCHAR(5),
	CONSTRAINT dia_horario_pk PRIMARY KEY ( dia_horario_id )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE turma (
	turma_id INTEGER NOT NULL AUTO_INCREMENT,
	materia_id INTEGER NOT NULL,
	professor_id INTEGER NOT NULL,
	data_limite_inscricao DATE NOT NULL,
	data_inicio DATE NOT NULL,
	data_final DATE NOT NULL,
	quantidade_vagas_disponivel INTEGER NOT NULL,
	CONSTRAINT turma_pk PRIMARY KEY ( turma_id )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE turma_dia_horario (
	turma_id INTEGER NOT NULL,
	dia_horario_id INTEGER NOT NULL,
	CONSTRAINT turma_dia_horario_pk PRIMARY KEY ( turma_id, dia_horario_id )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE status (
	status_id INTEGER NOT NULL AUTO_INCREMENT,
	descricao VARCHAR(20),
	CONSTRAINT status_pk PRIMARY KEY ( status_id )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE matricula (
	matricula_id INTEGER NOT NULL AUTO_INCREMENT,
	turma_id INTEGER NOT NULL,
	aluno_id INTEGER NOT NULL,
	status_id INTEGER NOT NULL,
	data_matricula DATE NOT NULL,
	data_trancamento DATE,
	data_cancelamento DATE,
	CONSTRAINT matricula_pk PRIMARY KEY ( matricula_id )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE usuario ADD CONSTRAINT usuario_fk_perfil FOREIGN KEY ( perfil_id ) REFERENCES perfil ( perfil_id );

ALTER TABLE usuario_permissao ADD CONSTRAINT usuario_permissao_fk_usuario FOREIGN KEY ( usuario_id ) REFERENCES usuario ( usuario_id );

ALTER TABLE aluno ADD CONSTRAINT aluno_fk_usuario FOREIGN KEY ( usuario_id ) REFERENCES usuario ( usuario_id );

ALTER TABLE aluno_curso ADD CONSTRAINT aluno_curso_fk_aluno FOREIGN KEY ( aluno_id ) REFERENCES aluno ( aluno_id );
ALTER TABLE aluno_curso ADD CONSTRAINT aluno_curso_fk_curso FOREIGN KEY ( curso_id ) REFERENCES curso ( curso_id );

ALTER TABLE materia_pre_requisito ADD CONSTRAINT materia_pre_requisito_fk_materia FOREIGN KEY ( materia_id ) REFERENCES materia ( materia_id );
ALTER TABLE materia_pre_requisito ADD CONSTRAINT materia_pre_requisito_fk_materiaprerequisito FOREIGN KEY ( materia_pre_requisito_id ) REFERENCES materia ( materia_id );

ALTER TABLE grade_curso_materia ADD CONSTRAINT grade_curso_materia_fk_grade FOREIGN KEY ( grade_id ) REFERENCES grade ( grade_id );
ALTER TABLE grade_curso_materia ADD CONSTRAINT grade_curso_materia_fk_curso FOREIGN KEY ( curso_id ) REFERENCES curso ( curso_id );
ALTER TABLE grade_curso_materia ADD CONSTRAINT grade_curso_materia_fk_materia FOREIGN KEY ( materia_id ) REFERENCES materia ( materia_id );

ALTER TABLE professor ADD CONSTRAINT professor_fk_usuario FOREIGN KEY ( usuario_id ) REFERENCES usuario ( usuario_id );

ALTER TABLE turma ADD CONSTRAINT turma_fk_materia FOREIGN KEY ( materia_id ) REFERENCES materia ( materia_id );
ALTER TABLE turma ADD CONSTRAINT turma_fk_professor FOREIGN KEY ( professor_id ) REFERENCES professor ( professor_id );

ALTER TABLE turma_dia_horario ADD CONSTRAINT turma_dia_horario_fk_turma FOREIGN KEY ( turma_id ) REFERENCES turma ( turma_id );
ALTER TABLE turma_dia_horario ADD CONSTRAINT turma_dia_horario_fk_dia_horario FOREIGN KEY ( dia_horario_id ) REFERENCES dia_horario ( dia_horario_id );

ALTER TABLE matricula ADD CONSTRAINT matricula_fk_turma FOREIGN KEY ( turma_id ) REFERENCES turma ( turma_id );
ALTER TABLE matricula ADD CONSTRAINT matricula_fk_aluno FOREIGN KEY ( aluno_id ) REFERENCES aluno ( aluno_id );
ALTER TABLE matricula ADD CONSTRAINT matricula_fk_status FOREIGN KEY ( status_id ) REFERENCES status ( status_id );

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

## Script para incluir registros na base de dados

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

USE pocacademico;

SET AUTOCOMMIT=0;

INSERT INTO pocacademico.perfil (descricao)
	VALUES ('administrador'),
	('professor'),
	('aluno');

INSERT INTO pocacademico.usuario (nome, senha, perfil_id)
	VALUES ('admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1),
	('Mário', '62ff73d2f8c839a544d575ccf11a80538dab905bc7045c3be9f214b53394be25s', 2),
	('José Augusto', '2a911c004ebd1d28a1faaf3a8f62b561769cd2580d1816668143eda388cbaeb5', 2),
	('Alexandre', '13e15721c9d4ad58d34983344dfba265a90d80f63db77c2eb3804379d9608889', 3),
	('Karla', 'adce40ea2bc3c83ce128ad6ebafcc478f5c447c67447a4bfc9bc342225a5bda0', 3);

INSERT INTO pocacademico.usuario_permissao (usuario_id, permissao)
	VALUES (1,'*');

INSERT INTO pocacademico.aluno (nome, data_nascimento, usuario_id)
	VALUES ('Alexandre', '1980-10-20', 4),
	('Karla', '1982-08-15', 5);

INSERT INTO pocacademico.curso (descricao)
	VALUES ('Ciência da Computação'),
	('Ciências Contábeis'),
	('Engenharia Civil'),
	('Medicina');

INSERT INTO pocacademico.aluno_curso (aluno_id, curso_id, data_inicio_curso, data_previsao_conclusao)
	VALUES (1, 1, '2006-01-01', '2011-11-01'),
	(2, 2, '2008-01-01', '2012-12-01');

INSERT INTO pocacademico.dia_horario (dia_semana, horario)
	VALUES ('segunda', '19:00'),
	('segunda', '20:00'),
	('segunda', '21:00'),
	('segunda', '22:00'),
	('terça', '19:00'),
	('terça', '20:00'),
	('terça', '21:00'),
	('terça', '22:00'),
	('quarta', '19:00'),
	('quarta', '20:00'),
	('quarta', '21:00'),
	('quarta', '22:00'),
	('quinta', '19:00'),
	('quinta', '20:00'),
	('quinta', '21:00'),
	('quinta', '22:00'),
	('sexta', '19:00'),
	('sexta', '20:00'),
	('sexta', '21:00'),
	('sexta', '22:00');

INSERT INTO pocacademico.grade (descricao, data_inicial, data_final)
	VALUES ('Grade ciência da computação 1', '2006-01-01', '2011-12-01'),
	('Grade ciências contábeis 1', '2008-01-01', '2012-12-01');

INSERT INTO pocacademico.materia (descricao) 
	VALUES ('AEDS 1'),
	('AEDS 2'),
	('Cálculo 1'),
	('Cálculo 2'),
	('Cálculo 3'),
	('Departamento pessoal'),
	('Fiscal');

INSERT INTO pocacademico.grade_curso_materia (grade_id, curso_id, materia_id) 
	VALUES (1, 1, 1);

INSERT INTO pocacademico.matricula (turma_id, aluno_id, status_id, data_matricula) 
	VALUES (1, 1, 3, '2006-01-24'),
		(2, 1, 3, '2017-09-16'),
		(3, 2, 3, '2017-09-16');

INSERT INTO pocacademico.professor (nome, data_nascimento, usuario_id) 
	VALUES ('Mário', '1969-12-31', 2),
	('José Augusto', '1959-12-31', 3);

INSERT INTO pocacademico.status (descricao) 
	VALUES ('Ativo'),
	('Inativo'),
	('Matriculado'),
	('Trancado'),
	('Cancelado');

INSERT INTO pocacademico.turma (materia_id, professor_id, data_limite_inscricao, data_inicio, data_final, quantidade_vagas_disponivel) 
	VALUES (1, 1, '2006-01-31', '2006-02-01', '2006-07-31', 50),
		(2, 1, '2018-01-31', '2018-02-01', '2018-07-31', 50),
		(3, 2, '2018-01-31', '2018-02-01', '2018-07-31', 40);

COMMIT;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

