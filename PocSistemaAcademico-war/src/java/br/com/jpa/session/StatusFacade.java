/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.session;

import br.com.jpa.entities.Status;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author alexandre
 */
@Stateless
public class StatusFacade extends AbstractFacade<Status> {

    @PersistenceContext(unitName = "PocSistemaAcademico-warPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public StatusFacade() {
        super(Status.class);
    }
    
    public List<Status> findByStatusId(int statusId) {
        List<Status> alunoList = em.createNamedQuery("Status.findByStatusId")
            .setParameter("statusId", statusId)
            .getResultList();
        return alunoList;
    }
    
}
