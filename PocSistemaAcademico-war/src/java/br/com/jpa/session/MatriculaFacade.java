/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.session;

import br.com.jpa.entities.Matricula;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 *
 * @author alexandre
 */
@Stateless
public class MatriculaFacade extends AbstractFacade<Matricula> {

    @PersistenceContext(unitName = "PocSistemaAcademico-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MatriculaFacade() {
        super(Matricula.class);
    }
    
    @Override
    public List<Matricula> findRange(int[] range) {
        Subject subject = SecurityUtils.getSubject();
        javax.persistence.Query q;
        if (subject.hasRole("administrador")) {
            javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
            cq.select(cq.from(Matricula.class));
            q = getEntityManager().createQuery(cq);
        } else {
            q = getEntityManager().createNamedQuery("Matricula.findAllByAluno");
            q.setParameter("usuarioNome", subject.getPrincipal());
        }
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

}
