/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.session;

import br.com.jpa.entities.Turma;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author alexandre
 */
@Stateless
public class TurmaFacade extends AbstractFacade<Turma> {

    @PersistenceContext(unitName = "PocSistemaAcademico-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TurmaFacade() {
        super(Turma.class);
    }
    
    public List findByQuantidadeVagasDisponivel(String usuarioNome) {
        List<Turma> turmaList = em.createNamedQuery("Turma.findByQuantidadeVagasDisponivel")
            .setParameter("usuarioNome", usuarioNome)
            .getResultList();
        return turmaList;
    }
    
}
