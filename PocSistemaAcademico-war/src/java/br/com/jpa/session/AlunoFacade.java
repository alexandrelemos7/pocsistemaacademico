/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.session;

import br.com.jpa.entities.Aluno;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author alexandre
 */
@Stateless
public class AlunoFacade extends AbstractFacade<Aluno> {

    @PersistenceContext(unitName = "PocSistemaAcademico-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AlunoFacade() {
        super(Aluno.class);
    }
    
    public List findByUsuarioNome(String usuarioNome) {
        List<Aluno> alunoList = em.createNamedQuery("Aluno.findByUsuarioNome")
            .setParameter("usuarioNome", usuarioNome)
            .getResultList();
        return alunoList;
    }

}
