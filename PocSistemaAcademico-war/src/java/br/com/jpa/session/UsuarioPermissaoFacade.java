/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.session;

import br.com.jpa.entities.UsuarioPermissao;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author alexandre
 */
@Stateless
public class UsuarioPermissaoFacade extends AbstractFacade<UsuarioPermissao> {

    @PersistenceContext(unitName = "PocSistemaAcademico-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioPermissaoFacade() {
        super(UsuarioPermissao.class);
    }
    
}
