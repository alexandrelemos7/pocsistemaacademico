/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.jpa.session;

import br.com.jpa.entities.DiaHorario;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author alexandre
 */
@Stateless
public class DiaHorarioFacade extends AbstractFacade<DiaHorario> {

    @PersistenceContext(unitName = "PocSistemaAcademico-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DiaHorarioFacade() {
        super(DiaHorario.class);
    }
    
}
