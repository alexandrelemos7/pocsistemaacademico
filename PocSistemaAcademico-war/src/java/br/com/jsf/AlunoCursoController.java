package br.com.jsf;

import br.com.jpa.entities.AlunoCurso;
import br.com.jsf.util.JsfUtil;
import br.com.jsf.util.PaginationHelper;
import br.com.jpa.session.AlunoCursoFacade;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@Named("alunoCursoController")
@SessionScoped
public class AlunoCursoController implements Serializable {

    private AlunoCurso current;
    private DataModel items = null;
    @EJB
    private br.com.jpa.session.AlunoCursoFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public AlunoCursoController() {
    }

    public AlunoCurso getSelected() {
        if (current == null) {
            current = new AlunoCurso();
            current.setAlunoCursoPK(new br.com.jpa.entities.AlunoCursoPK());
            selectedItemIndex = -1;
        }
        return current;
    }

    private AlunoCursoFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (AlunoCurso) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new AlunoCurso();
        current.setAlunoCursoPK(new br.com.jpa.entities.AlunoCursoPK());
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            current.getAlunoCursoPK().setAlunoId(current.getAluno().getAlunoId());
            current.getAlunoCursoPK().setCursoId(current.getCurso().getCursoId());
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("AlunoCursoCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (AlunoCurso) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            current.getAlunoCursoPK().setAlunoId(current.getAluno().getAlunoId());
            current.getAlunoCursoPK().setCursoId(current.getCurso().getCursoId());
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("AlunoCursoUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (AlunoCurso) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("AlunoCursoDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public AlunoCurso getAlunoCurso(br.com.jpa.entities.AlunoCursoPK id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = AlunoCurso.class)
    public static class AlunoCursoControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            AlunoCursoController controller = (AlunoCursoController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "alunoCursoController");
            return controller.getAlunoCurso(getKey(value));
        }

        br.com.jpa.entities.AlunoCursoPK getKey(String value) {
            br.com.jpa.entities.AlunoCursoPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new br.com.jpa.entities.AlunoCursoPK();
            key.setAlunoId(Integer.parseInt(values[0]));
            key.setCursoId(Integer.parseInt(values[1]));
            return key;
        }

        String getStringKey(br.com.jpa.entities.AlunoCursoPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getAlunoId());
            sb.append(SEPARATOR);
            sb.append(value.getCursoId());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof AlunoCurso) {
                AlunoCurso o = (AlunoCurso) object;
                return getStringKey(o.getAlunoCursoPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + AlunoCurso.class.getName());
            }
        }

    }

}
