package br.com.jsf;

import br.com.jpa.entities.Status;
import br.com.jsf.util.JsfUtil;
import br.com.jsf.util.PaginationHelper;
import br.com.jpa.session.StatusFacade;
import br.com.util.Constantes;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

@Named("statusController")
@SessionScoped
public class StatusController implements Serializable {

    private Status current;
    private DataModel items = null;
    @EJB
    private br.com.jpa.session.StatusFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public StatusController() {
    }

    public Status getSelected() {
        if (current == null) {
            current = new Status();
            selectedItemIndex = -1;
        }
        return current;
    }

    private StatusFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Status) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Status();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("StatusCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Status) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("StatusUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Status) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/resources/Bundle").getString("StatusDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/resources/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        Subject subject = SecurityUtils.getSubject();
        Session session = subject.getSession();
        int perfilId = (int) session.getAttribute(Constantes.SESSION_PERFIL_ID);
        if (perfilId != 0 && perfilId == Constantes.PEFIL_ID_ADMINISTRADOR)
            return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
        else
            return JsfUtil.getSelectItems(ejbFacade.findByStatusId(Constantes.STATUS_ID_MATRICULADO), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        Subject subject = SecurityUtils.getSubject();
        Session session = subject.getSession();
        int perfilId = (int) session.getAttribute(Constantes.SESSION_PERFIL_ID);
        if (perfilId != 0 && perfilId == Constantes.PEFIL_ID_ADMINISTRADOR)
            return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
        else
            return JsfUtil.getSelectItems(ejbFacade.findByStatusId(Constantes.STATUS_ID_MATRICULADO), true, false);
    }

    public Status getStatus(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Status.class)
    public static class StatusControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            StatusController controller = (StatusController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "statusController");
            return controller.getStatus(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Status) {
                Status o = (Status) object;
                return getStringKey(o.getStatusId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Status.class.getName());
            }
        }

    }

}
