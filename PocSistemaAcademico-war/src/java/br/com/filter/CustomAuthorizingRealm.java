/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.filter;

import br.com.jpa.entities.Aluno;
import br.com.jpa.session.AlunoFacade;
import javax.ejb.EJB;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAccount;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

//import com.test.db.DaoUtils;
//import com.test.entity.User;
/**
 *
 * @author alexandre
 */
public class CustomAuthorizingRealm extends AuthorizingRealm {

    @EJB
    private br.com.jpa.session.AlunoFacade ejbFacade;
    
    private AlunoFacade getFacade() {
        return ejbFacade;
    }
    
    protected SimpleAccount getAccount(String id) {
        Aluno user = getFacade().find(Integer.parseInt(id));
//        SimpleAccount account = new SimpleAccount(user.getEmail(), user.getPassword(), getName());
        SimpleAccount account = new SimpleAccount(user.getAlunoId(), user.getNome(), getName());
        return account;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        String username = (String) getAvailablePrincipal(principalCollection);
        return getAccount(username);
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        UsernamePasswordToken upToken = (UsernamePasswordToken) token;
        return getAccount(upToken.getUsername());
    }
}
