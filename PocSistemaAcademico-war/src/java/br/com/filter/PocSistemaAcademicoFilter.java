/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.shiro.SecurityUtils;

/**
 *
 * @author alexandre
 */
@WebFilter("/PocSistemaAcademicoFilter")
public class PocSistemaAcademicoFilter implements Filter {

    private ServletContext context;

    public void init(FilterConfig fConfig) throws ServletException {
        this.context = fConfig.getServletContext();
        this.context.log("AuthenticationFilter initialized");
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String uri = req.getRequestURI();
        this.context.log("Requested Resource::"+uri);

        HttpSession session = req.getSession(false);

        Object securityUtils = SecurityUtils.getSubject().getPrincipal();
        
//        if(session == null && !(uri.endsWith("html") || uri.endsWith("LoginServlet"))){
        if (securityUtils == null && !(uri.substring(uri.lastIndexOf("/")+1).equals("index.xhtml") || uri.matches(".*(css|jpg|png|gif|js)"))) {
            this.context.log("Unauthorized access request");
            res.sendRedirect(req.getContextPath()+"/faces/index.xhtml");
        }else{
            // pass the request along the filter chain
            chain.doFilter(request, response);
        }

    }



    public void destroy() {
        //close any resources here
    }

}
