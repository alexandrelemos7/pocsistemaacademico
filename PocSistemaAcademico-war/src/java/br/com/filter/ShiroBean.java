/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.filter;

/**
 *
 * @author alexandre
 */
import br.com.jpa.entities.Usuario;
import br.com.jpa.session.UsuarioFacade;
import br.com.util.Constantes;
import javax.ejb.EJB;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.subject.Subject;

import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.apache.shiro.session.Session;

@ManagedBean
@RequestScoped
public class ShiroBean {

    private String username, password;
    private boolean rememberMe;
    
    @EJB
    private UsuarioFacade ejbUsuarioFacade;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public void loginUser(){

        try{

            Subject currentUser         = SecurityUtils.getSubject();
            UsernamePasswordToken token = new UsernamePasswordToken(username, new Sha256Hash(password).toHex());

            token.setRememberMe(rememberMe);
            currentUser.login(token);
            
            Usuario usuario = ejbUsuarioFacade.findByNome(username);
            
            Session session = currentUser.getSession();
            session.setAttribute(Constantes.SESSION_PERFIL_ID, usuario.getPerfilId().getPerfilId());
            
        } catch (UnknownAccountException uae ) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login failed!", "Your username wrong"));

        } catch (IncorrectCredentialsException ice ) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login failed!", "Password is incorrect"));

        } catch (LockedAccountException lae ) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login failed!", "This username is locked"));

        } catch(AuthenticationException aex){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login failed!", aex.toString()));
        }

    }

    public void authorizedUserControl(){

        Subject currentUser = SecurityUtils.getSubject();
        if(null != currentUser.getPrincipal()){

            NavigationHandler nh =  FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
            if (currentUser.hasRole("administrador"))
                nh.handleNavigation(FacesContext.getCurrentInstance(), null, "/faces/aluno/List.xhtml?faces-redirect=true");
            else
                nh.handleNavigation(FacesContext.getCurrentInstance(), null, "/faces/matricula/List.xhtml?faces-redirect=true");

        }
    }
    
    public void logout() {
        SecurityUtils.getSubject().logout();
        
        NavigationHandler nh =  FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        nh.handleNavigation(FacesContext.getCurrentInstance(), null, "/faces/index.xhtml?faces-redirect=true");
    }
}